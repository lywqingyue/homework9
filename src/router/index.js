import Vue from 'vue'
import VueRouter from 'vue-router'
import CampusBinhai from '../views/CampusBinhai.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/CampusBinhai',
    name: 'CampusBinhai',
    component: CampusBinhai
  },
  {
    path: '/CampusChashan',
    name: 'CampusChashan',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/CampusChashan.vue')
    }
  },
  {
    path: '/CampusYueqing',
    name: 'CampusYueqing',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "student" */ '../views/CampusYueqing.vue')
    }
  },
  {path: '/studentsystem', redirect: '/login'},
  {
    path: '/Login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "student" */ '../views/Login.vue')
    }
  },
  {
    path: '/StudentSystem',
    name: 'StudentSystem',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "student" */ '../views/StudentSystem.vue')
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
